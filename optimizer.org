* Flow
  SCHEDULED: <2016-09-23 03:27> DEADLINE: <2016-09-30 05:26>
** Get parameter templates
*** parameters_template.py -> retrieve_template
** Get all non-excluded PNRs
*** retrieve_from_cache -> retrieve_impacted_pnrs
** Create dictionary where key is PNR and value is list of flight alternatives
*** BusAFS::retrieve_alternative_solutions OR bus_afs -> BusAFS.retrieve_alternative_solutions_on_rerun
**** retrieve_alternative_solutions
***** get_travel_solutions -> GetTravelSolutions
      Create the set of all different itineraries
Make the call to the db with this set in input
***** from_to_matching -> FromToMatching
      Perform some checks. Not fully understood yet
It seems to be a checking class. It raises an FromToMatchingException under various conditions.
***** Remap the found TravelSolutions to every PNR
**** TODO retrieve_alternative_solutions_on_rerun
*** GetResMappingCabin::getMapping()
    Only applied if allow_other_airline
For every segment of From and To-flights, get the mappings cabin code -> cabin class
Gets 2 mappings: one from RES and one from INV
*** BusAvailability::retrieve_leg_details()
**** BusAvailability::_process_segment_date_in_itinerary()
     Populate flight_dict: key=flight -> value=dict of list of list of legs by segment key
Call IRBKRQ
**** BusAvailability::_multiple_call_process()
     Same as above for the remaining flights
**** BusAvailability::_adjust_to_leg_cabin_availability
***** Build a (segment, cabin_code)->NumberPassenger dictionnary: from_segment_cabin_av
      Iterate on all PNR and on all segment and increment the dict by PNR::nb_party for the given cabine_code
***** Assign the corresponding cabin_availability for each leg of all ToFlights
**** BusAvailability::_adjust_to_leg_ssr_availability()
     Compute ssr availability for every ToFlights
**** For each segment of all TravelSolutions assign the corresponding leg list computed above
*** BusCabinsMatching::execute()
    filter TravelSolutions that meet the cabins criteria
*** BusSsrMatching::execute()
    Populate the Special Service Requirements for the TravelSolution
(= make copy of the ones from the FromSegments)
*** BusAFS::filter_alternative_solutions()
*** BusAFS::filter_based_on_ssr()
*** DbaPnr::save_problem()
    Save on DB
*** Optimization
    Choose between HEURISTIC and LINEAR
**** HEURISTIC path: Optimizer::optimize()
***** Loop on every PNR ordered by priority (I suggest to sort them also by nb_party)
****** For each PNR, sort by TravelSolution weight
******* weight function:
        #+BEGIN_SRC latex
\begin{equation}
nb_{party} * \sign{priority_{pax}} * \abs{priority_{pax}}^{inclination_{priority}} * (inclination_{airline} * (cost^{overnight} + reacc^{cost})
           + (1 - inclination_{airline}) * solution_weight)
#+END_SRC
******** pax_priority
         #+BEGIN_SRC latex
priority_{pax} = score * template_{pnr score} + bias_{total}
#+END_SRC
******** reacc_cost
         Can be set to 0 with the boolean 'reacc_cost_sum'
#+BEGIN_SRC latex
reacc_{cost}=\sum \frac{dist_{leg}}{\sum dist_{leg} } * cost_{reacc availabality}
#+END_SRC
where cost_{reacc availabality}:
#+BEGIN_SRC latex
cost_{reacc availabality} = c1 a +d1 if a \in [0, A1] \\
                     & = c2 a +d2 if a \in [A1, A2] \\
                     & = c3 a +d3 if a \in [A2, 100\%]
#+END_SRC
with A1 and A2 the breakpoints in availability
******** inclination_{priority} is a template parameter
******** inclination_{airline} is a template parameter
******** cost_{overnight} is template parameter
******* Check for the first valid solution
******** Loop on every leg of every segment
         Return False if one requirement is not present
- Check that 'cabin.availability < pnr.nb_party'
- Check that ssr quota is not exceeded if ssr exist.
******* When a solution is found
******** the following values are updated
         - reacc_pax
- reacc_pnr
- total_weight
******* Don't look at other solution when one is found
*** Compute KPI (Key Performance Indexes)
*** Add KPIs to DB
*** Update PNR solutions
*** Reset excluded Pnrs solution chosen
* Questions
  - In BusAFS, if FromToMatching raises an exception, why trying alternative by alternative ?
- Why does getMapping is only applied when allow_other_airline ?
