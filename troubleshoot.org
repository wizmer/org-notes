* Test tools
- If batchId = 0, then run:
  cmk admin set var 'OPR_REFERENCE_AIRLINE_CODE' -val 'WN' -app APK

- 

* Troubleshooting

** Problem: warning: unable to unlink: Invalid argument | Text file in use
Solution
manage stopgsv or close TestTool Server and retry GIT command

** Problem: Failed to run SI commands
When launching commands for ‘After non reg test, if no data’
[ ERROR ] Failed to run SI commands:
Error: Master Agent environment variable SI_MASTERADMCLT_SECURED_IP_PORT not set.
Solution
source .manageenv	

** [ ERROR ] Step 1 - clean_folder - Failed!
[ ERROR ] Please install in an empty folder

Error! Can't install. Tip: to stop the BE, type manage stop.backend


* Database:
Open with:
rlwrap sqlplus ownngibcoste/amadeus@nce14bls

and then:
alter session set current_schema=ownngibcoste;


